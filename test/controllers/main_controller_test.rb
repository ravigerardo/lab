require 'test_helper'

class MainControllerTest < ActionDispatch::IntegrationTest
  test "should get mision" do
    get main_mision_url
    assert_response :success
  end

  test "should get contacto" do
    get main_contacto_url
    assert_response :success
  end

end
