# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_03_064240) do

  create_table "contactos", force: :cascade do |t|
    t.string "nombre"
    t.string "puesto"
    t.string "telefono"
    t.string "email"
    t.string "horario"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "horarios", force: :cascade do |t|
    t.string "dia"
    t.string "hora"
    t.integer "laboratorio_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "matter_id"
    t.string "asunto"
    t.text "nota"
    t.integer "user_id"
    t.index ["laboratorio_id", "dia", "hora"], name: "index_horarios_on_laboratorio_id_and_dia_and_hora", unique: true
    t.index ["laboratorio_id"], name: "index_horarios_on_laboratorio_id"
    t.index ["matter_id"], name: "index_horarios_on_matter_id"
  end

  create_table "laboratorios", force: :cascade do |t|
    t.string "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ubicacion"
    t.integer "equipos"
  end

  create_table "matters", force: :cascade do |t|
    t.string "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_matters_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "nombre"
    t.boolean "is_admin?", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
