class AddUniqueIndexToHorarios < ActiveRecord::Migration[5.2]
  def change
    add_index :horarios, [:laboratorio_id, :dia, :hora], unique: true
  end
end
