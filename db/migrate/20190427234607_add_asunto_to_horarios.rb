class AddAsuntoToHorarios < ActiveRecord::Migration[5.2]
  def change
    add_column :horarios, :asunto, :string
    add_column :horarios, :nota, :text
  end
end
