class AddUbicacionToLaboratorios < ActiveRecord::Migration[5.2]
  def change
    add_column :laboratorios, :ubicacion, :string
    add_column :laboratorios, :equipos, :integer
  end
end
