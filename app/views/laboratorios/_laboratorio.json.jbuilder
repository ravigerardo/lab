json.extract! laboratorio, :id, :nombre, :created_at, :updated_at
json.url laboratorio_url(laboratorio, format: :json)
