#json.partial! "horarios/horario", horario: @horario
semana = {lu: 'Lunes', ma: 'Martes', mi: 'Miércoles', ju: 'Jueves', vi: 'Viernes', sa: 'Sábado'}
dia = semana[:"#{@horario.dia}"]
json.dia dia
json.hora @horario.hora
json.laboratorio @horario.laboratorio.nombre
json.asunto @horario.asunto
json.nota @horario.nota
json.maestro  @horario.user_id ? User.find(@horario.user_id).nombre : 'Maestro no asignado'

