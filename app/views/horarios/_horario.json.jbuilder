json.extract! horario, :id, :dia, :hora, :laboratorio_id, :asunto, :nota, :created_at, :updated_at
json.url horario_url(horario, format: :json)
