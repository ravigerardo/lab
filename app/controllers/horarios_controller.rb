class HorariosController < ApplicationController
  before_action :authenticate_user!, only: [:new, :edit, :create, :update, :destroy]
  skip_before_action :verify_authenticity_token
  before_action :set_horario, only: [:show, :edit, :update, :destroy]
  before_action :horarios_active

  # GET /horarios
  # GET /horarios.json
  def index
    @horarios = Horario.all
  end

  # GET /horarios/1
  # GET /horarios/1.json
  def show
  end

  # GET /horarios/new
  def new
    @horario = Horario.new
  end

  # GET /horarios/1/edit
  def edit
  end

  # POST /horarios
  # POST /horarios.json
  def create
    @horario = Horario.new(horario_params)

    respond_to do |format|
      if @horario.save
        format.html { redirect_to @horario, notice: 'Horario was successfully created.' }
        format.json { render :show, status: :created, location: @horario }
      else
        format.html { render :new }
        format.json { render json: @horario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /horarios/1
  # PATCH/PUT /horarios/1.json
  def update
    if @horario.user_id == nil || @horario.user_id == current_user.id || current_user.is_admin?
      params[:user_id] && current_user.is_admin? ? @horario.user_id = params[:user_id] : @horario.user_id = current_user.id

      respond_to do |format|
        if @horario.update(horario_params)
          format.html { redirect_to "/laboratorios/#{@horario.laboratorio_id}", notice: 'Horario actualizado' }
          format.json { render :show, status: :ok, location: @horario }
        end
      end

    else
      redirect_to "/laboratorios/#{@horario.laboratorio_id}", alert: 'Horario ocupado' 
    end
  end

  # DELETE /horarios/1
  # DELETE /horarios/1.json
  def destroy
    laboratorio_id = @horario.laboratorio_id
    @horario.update(asunto: nil, nota: nil, user_id: nil)
    respond_to do |format|
      format.html { redirect_to "/laboratorios/#{laboratorio_id}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_horario
      @horario = Horario.find(params[:id])
    end

    def horarios_active
      @horarios_active = 'active'
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def horario_params
      params.require(:horario).permit(:dia, :hora, :laboratorio_id, :asunto, :nota)
    end
end
