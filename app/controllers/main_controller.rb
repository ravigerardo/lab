class MainController < ApplicationController
  def mision
    @mision_active = 'active'
  end

  def contacto
    @contacto_active = 'active'
    @contactos = Contacto.all
  end

  def create_contacto
    contacto = Contacto.new(contacto_params)

    if contacto.save
      redirect_to '/contacto', notice: 'Contacto registrado correctamente'
    else
      redirect_to '/contacto', notice: 'Error al registrar el contacto'
    end
  end

  def delete_contacto
    contacto = Contacto.find(params[:id])
    contacto.delete
    redirect_to '/contacto', notice: 'Contacto eliminado correctamente'
  end

  private
  def contacto_params
    params.require(:contacto).permit(:nombre, :puesto, :telefono, :email, :horario)
  end
end
