class LaboratoriosController < ApplicationController
  before_action :have_permissions?, only: [:new, :edit, :create, :update, :destroy]
  before_action :set_laboratorio, only: [:show, :edit, :update, :destroy, :reset_horario]
  before_action :laboratorios_active

  # GET /laboratorios
  # GET /laboratorios.json
  def index
    @laboratorios = Laboratorio.all
  end

  # GET /laboratorios/1
  # GET /laboratorios/1.json
  def show
    horarios = Horario.where(laboratorio_id: @laboratorio.id)
    @horario = Hash.new
    @users = User.all
    
    horarios.each do |h|
      key = "#{h[:dia]}#{h[:hora].gsub '-', ''}"
      @horario[:"#{key}"] = { id: h.id, asunto: h.asunto, user_id: h.user_id }
    end

    #raise @horario.to_yaml
  end

  # GET /laboratorios/new
  def new
    @laboratorio = Laboratorio.new
  end

  # GET /laboratorios/1/edit
  def edit
  end

  # POST /laboratorios
  # POST /laboratorios.json
  def create
    @laboratorio = Laboratorio.new(laboratorio_params)
    horario = [
      {dia: 'lu', hora: '7-8'}, {dia: 'lu', hora: '8-9'}, {dia: 'lu', hora: '9-10'}, {dia: 'lu', hora: '10-11'}, {dia: 'lu', hora: '11-12'}, {dia: 'lu', hora: '12-13'}, {dia: 'lu', hora: '13-14'}, {dia: 'lu', hora: '14-15'}, {dia: 'lu', hora: '15-16'}, {dia: 'lu', hora: '16-17'}, {dia: 'lu', hora: '17-18'}, {dia: 'lu', hora: '18-19'}, {dia: 'lu', hora: '19-20'}, {dia: 'lu', hora: '20-21'}, {dia: 'lu', hora: '21-22'},
      {dia: 'ma', hora: '7-8'}, {dia: 'ma', hora: '8-9'}, {dia: 'ma', hora: '9-10'}, {dia: 'ma', hora: '10-11'}, {dia: 'ma', hora: '11-12'}, {dia: 'ma', hora: '12-13'}, {dia: 'ma', hora: '13-14'}, {dia: 'ma', hora: '14-15'}, {dia: 'ma', hora: '15-16'}, {dia: 'ma', hora: '16-17'}, {dia: 'ma', hora: '17-18'}, {dia: 'ma', hora: '18-19'}, {dia: 'ma', hora: '19-20'}, {dia: 'ma', hora: '20-21'}, {dia: 'ma', hora: '21-22'},
      {dia: 'mi', hora: '7-8'}, {dia: 'mi', hora: '8-9'}, {dia: 'mi', hora: '9-10'}, {dia: 'mi', hora: '10-11'}, {dia: 'mi', hora: '11-12'}, {dia: 'mi', hora: '12-13'}, {dia: 'mi', hora: '13-14'}, {dia: 'mi', hora: '14-15'}, {dia: 'mi', hora: '15-16'}, {dia: 'mi', hora: '16-17'}, {dia: 'mi', hora: '17-18'}, {dia: 'mi', hora: '18-19'}, {dia: 'mi', hora: '19-20'}, {dia: 'mi', hora: '20-21'}, {dia: 'mi', hora: '21-22'},
      {dia: 'ju', hora: '7-8'}, {dia: 'ju', hora: '8-9'}, {dia: 'ju', hora: '9-10'}, {dia: 'ju', hora: '10-11'}, {dia: 'ju', hora: '11-12'}, {dia: 'ju', hora: '12-13'}, {dia: 'ju', hora: '13-14'}, {dia: 'ju', hora: '14-15'}, {dia: 'ju', hora: '15-16'}, {dia: 'ju', hora: '16-17'}, {dia: 'ju', hora: '17-18'}, {dia: 'ju', hora: '18-19'}, {dia: 'ju', hora: '19-20'}, {dia: 'ju', hora: '20-21'}, {dia: 'ju', hora: '21-22'},
      {dia: 'vi', hora: '7-8'}, {dia: 'vi', hora: '8-9'}, {dia: 'vi', hora: '9-10'}, {dia: 'vi', hora: '10-11'}, {dia: 'vi', hora: '11-12'}, {dia: 'vi', hora: '12-13'}, {dia: 'vi', hora: '13-14'}, {dia: 'vi', hora: '14-15'}, {dia: 'vi', hora: '15-16'}, {dia: 'vi', hora: '16-17'}, {dia: 'vi', hora: '17-18'}, {dia: 'vi', hora: '18-19'}, {dia: 'vi', hora: '19-20'}, {dia: 'vi', hora: '20-21'}, {dia: 'vi', hora: '21-22'},
      {dia: 'sa', hora: '7-8'}, {dia: 'sa', hora: '8-9'}, {dia: 'sa', hora: '9-10'}, {dia: 'sa', hora: '10-11'}, {dia: 'sa', hora: '11-12'}, {dia: 'sa', hora: '12-13'}, {dia: 'sa', hora: '13-14'}, {dia: 'sa', hora: '14-15'}, {dia: 'sa', hora: '15-16'}, {dia: 'sa', hora: '16-17'}, {dia: 'sa', hora: '17-18'}, {dia: 'sa', hora: '18-19'}, {dia: 'sa', hora: '19-20'}, {dia: 'sa', hora: '20-21'}, {dia: 'sa', hora: '21-22'}
    ]
    horario.each do |h|
      @laboratorio.horarios.new(dia: h[:dia], hora: h[:hora])
    end

    respond_to do |format|
      if @laboratorio.save
        format.html { redirect_to @laboratorio, notice: 'Laboratorio creado' }
        format.json { render :show, status: :created, location: @laboratorio }
      else
        format.html { render :new }
        format.json { render json: @laboratorio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /laboratorios/1
  # PATCH/PUT /laboratorios/1.json
  def update
    respond_to do |format|
      if @laboratorio.update(laboratorio_params)
        format.html { redirect_to @laboratorio, notice: 'Laboratorio actualizado' }
        format.json { render :show, status: :ok, location: @laboratorio }
      else
        format.html { render :edit }
        format.json { render json: @laboratorio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /laboratorios/1
  # DELETE /laboratorios/1.json
  def destroy
    @laboratorio.horarios.destroy_all
    @laboratorio.destroy
    respond_to do |format|
      format.html { redirect_to laboratorios_url, notice: 'Laboratorio eliminado' }
      format.json { head :no_content }
    end
  end

  def reset_horario
    @laboratorio.horarios.update_all(asunto: nil, nota: nil, user_id: nil)
    respond_to do |format|
      format.html { redirect_to @laboratorio }
      format.json { head :no_content }
    end
  end

  private
    def have_permissions?
        redirect_to root_path, alert: 'No eres administrador' unless user_signed_in? && current_user.is_admin?
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_laboratorio
      @laboratorio = Laboratorio.find(params[:id])
    end

    def laboratorios_active
      @laboratorios_active = 'active'
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def laboratorio_params
      params.require(:laboratorio).permit(:nombre, :ubicacion, :equipos)
    end
end
