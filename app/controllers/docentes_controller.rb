class DocentesController < ApplicationController
    before_action :have_permissions?
    before_action :set_docente, only: :destroy
    before_action :docentes_active

    def index
        @docentes = User.where(is_admin?: false)
    end

    def create
        user = User.new(user_params)
        user.password = '123456'
        if user.save
            redirect_to docentes_path, notice: 'Docente creado correctamente'
        else
            redirect_to docentes_path, alert: 'El nombre y el correo eléctronico son obligatorios'
        end
    end

    def destroy
        @docente.destroy
        respond_to do |format|
        format.html { redirect_to docentes_url, notice: 'Docente eliminado' }
        format.json { head :no_content }
        end
    end

    private

    def have_permissions?
        redirect_to root_path, alert: 'No eres administrador' unless user_signed_in? && current_user.is_admin?
    end

    def docentes_active
        @docentes_active = 'active'
    end

    def set_docente
        @docente = User.find(params[:id])
    end

    def user_params
        params.require(:user).permit(:nombre, :email)
    end
end
