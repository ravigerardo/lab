class Horario < ApplicationRecord
  belongs_to :laboratorio
  #belongs_to :matter
  validates :laboratorio_id, uniqueness: { scope: [:dia, :hora], message: "Ya existe una clase en este laboratorio con el mismo horario" }
end
