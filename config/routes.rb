Rails.application.routes.draw do
  root 'laboratorios#index'
  
  get 'mision-vision', to: 'main#mision'
  get 'contacto', to: 'main#contacto'
  post 'contacto', to: 'main#create_contacto'
  delete 'contacto/:id', to: 'main#delete_contacto'

  delete 'laboratorios/:id/horario', to: 'laboratorios#reset_horario'

  devise_for :users
  resources :docentes, only: [:index, :create, :destroy]
  resources :matters
  resources :horarios
  resources :laboratorios
end
